package filesystem;

import java.util.LinkedList;
import java.util.Queue;

public class Buffer extends File {
    private int MAX_BUF_FILE_SIZE;
    private Queue<String> content;

    public Buffer(String name, int max_buf_size) {
        super(name);
        this.MAX_BUF_FILE_SIZE = max_buf_size;
        this.content = new LinkedList<>();
    }

    public void pushElement(String line) throws Exception {
        try {
            mutex.lock();
            if (content.size() >= MAX_BUF_FILE_SIZE)
                throw new Exception("no free space in buffer");
            content.add(line);
        } finally {
            mutex.unlock();
        }
    }

    public String consumeElement() {
        return content.poll();
    }
}
