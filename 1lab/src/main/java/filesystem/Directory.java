package filesystem;

import java.util.ArrayList;
import java.util.List;

public class Directory extends File {
    private List<File> children;
    private int DIR_MAX_ELEMS;

    public Directory(String name, int directoryMaxElements) {
        super(name);
        DIR_MAX_ELEMS = directoryMaxElements;
        children = new ArrayList<>();
    }

    public List<File> getContent() {
        try {
            mutex.lock();
            return children;
        } finally {
            mutex.unlock();
        }
    }

    public void insertFile(File child) throws Exception {
        try {
            mutex.lock();
            if (children.size() >= DIR_MAX_ELEMS)
                throw new Exception("not enough space");
            children.add(child);
            child.setParentDirectory(this);
        } finally {
            mutex.unlock();
        }
    }

    public void deleteFile(File child) {
        try {
            mutex.lock();
            children.remove(child);
        } finally {
            mutex.unlock();
        }
    }

    public void printTree(int depth) {
        String separator = "    ";
        System.out.println(separator.repeat(depth) + "/" + getDescription());
        for (File child : children) {
            if (child instanceof Directory) {
                ((Directory) child).printTree(depth + 1);
            } else {
                System.out.println(separator.repeat(depth + 1) + child.getDescription());
            }
        }
    }
}
