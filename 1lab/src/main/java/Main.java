import filesystem.*;
import threads.Thread;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Directory root = new Directory("home", 10);
        try {
            root.insertFile(new Buffer("buf_file", 20));
            root.insertFile(new TrashBin("Recycle Bin"));
            Directory dir1 = new Directory("sem2", 10);
            root.insertFile(dir1);
            dir1.insertFile(new BinaryFile("bin_file"));
            Directory dir2 = new Directory("lab1", 10);
            dir1.insertFile(dir2);
            dir2.insertFile(new LogTextFile("text_file"));
            root.printTree(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Thread T3 = new Thread( "Thread - 3 ", null, null);
        Thread T4 = new Thread( "Thread - 4 ", null, null);
        ArrayList<Thread> array = new ArrayList<>();
        array.add(T3); array.add(T4);
        Thread T1 = new Thread( "Thread - 1 ", null, array);
        Thread T2 = new Thread( "Thread - 2 ", null, null);
        array.add(T1); array.add(T2);
        Thread T5 = new Thread( "Thread - 5 ", array, null);

        T1.start();
        T2.start();
        T3.start();
        T4.start();
        T5.start();

        try {
            T1.join();
            T2.join();
            T3.join();
            T4.join();
            T5.join();
        } catch ( Exception e) {
            System.out.println("Interrupted");
        }

    }
}
