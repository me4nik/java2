package filesystem;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class BinaryFileTest {

    @Test
    void readEmptyTest() {
        BinaryFile file = new BinaryFile("file.bin");
        assertEquals("", file.read());

        file = new BinaryFile("file.bin", "");
        assertEquals("", file.read());
    }

    @Test
    void readTest() {
        BinaryFile file = new BinaryFile("file.bin", "file");
        assertEquals("file", file.read());
    }
}