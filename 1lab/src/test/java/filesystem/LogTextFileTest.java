package filesystem;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class LogTextFileFileTest {

    @Test
    void writeAndReadTest() {
        LogTextFile file = new LogTextFile("data.log");
        assertEquals("", file.read());

        String[] lines = new String[] {"first", "second", "third"};
        for (String line: lines) {
            file.appendLine(line);
        }

        String expectedContent = String.join("\n", lines);
        assertEquals(expectedContent, file.read());
        assertEquals(expectedContent, file.read());
    }
}