package filesystem;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class DirectoryTest {

    @Test
    void getContentEmptyTest() {
        Directory root = new Directory("root", 10);
        assertEquals(Collections.emptyList(), root.getContent());
    }

    @Test
    void insertAndGetContentTest() {
        Directory root = new Directory("root", 10);
        File binFile = new BinaryFile("text.txt");
        File bufFile  = new Buffer("buffer_file.bf", 10);
        Directory dataDir = new Directory("data", 10);
        Directory logsDir = new Directory("logs", 10);
        File logFile = new LogTextFile("logs.log");

        try {
            root.insertFile(binFile);
            root.insertFile(bufFile);
            root.insertFile(dataDir);
            dataDir.insertFile(logsDir);
            logsDir.insertFile(logFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(Arrays.asList(binFile, bufFile, dataDir), root.getContent());
        assertEquals(Collections.singletonList(logsDir), dataDir.getContent());
        assertEquals(Collections.singletonList(logFile), logsDir.getContent());
    }

    @Test
    void insertAndDeleteTest() {
        Directory root = new Directory("root", 10);
        Directory dataDir = new Directory("data", 10);
        File bufFile  = new Buffer("buffer_file.bf", 10);
        File binFile = new BinaryFile("text.txt");

        try {
            root.insertFile(dataDir);
            dataDir.insertFile(bufFile);
            dataDir.insertFile(binFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        bufFile.delete();
        assertEquals(Collections.singletonList(binFile), dataDir.getContent());

        dataDir.delete();
        assertEquals(Collections.emptyList(), root.getContent());
    }
}