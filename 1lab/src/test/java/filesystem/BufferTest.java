package filesystem;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class BufferTest {

    @Test
    void writeAndReadTest() {
        Buffer file = new Buffer("buffer", 10);
        assertNull(file.consumeElement());

        try {
            file.pushElement("first");
            file.pushElement("second");
            file.pushElement("third");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("first", file.consumeElement());
        assertEquals("second", file.consumeElement());
        assertEquals("third", file.consumeElement());
        assertNull(file.consumeElement());
    }
}